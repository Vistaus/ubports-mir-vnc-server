import atexit
import os
import pyotherside
import signal
import subprocess

from threading import Thread


def start(displayId, viewOnly, width, height, refreshrate, rotation):
    vnc_process = subprocess.Popen(
        [
            "./vncserver.sh",
            "-m",
            "/run/mir_socket",
            "-d",
            displayId,
            viewOnly,
            "-s",
            width,
            height,
            "--cap-interval",
            refreshrate,
            "-o",
            rotation,
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        preexec_fn=os.setsid,
    )

    thread = Thread(target=log_worker, args=[vnc_process.stdout])
    thread.daemon = True
    thread.start()

    thread.join(timeout=1)
    return vnc_process


def setBacklight(value):
    # int(value / 255 * 100) because max brightness is 255 and mirbacklight expects an integer percentage
    subprocess.call(["/usr/bin/mirbacklight", str(int(value / 255 * 100))])


def log_worker(stdout):
    """ needs to be in a thread so we can read the stdout w/o blocking """
    for stdout_line in iter(stdout.readline, ""):
        if len(stdout_line) > 0:
            pyotherside.send("log", stdout_line)


def is_running(vnc_process):
    # None indicates process is active
    return vnc_process.poll() is None


@atexit.register
def stop(vnc_process):
    # Ensure all VNC server processes are terminated (https://stackoverflow.com/a/4791612)
    if is_running(vnc_process):
        os.killpg(os.getpgid(vnc_process.pid), signal.SIGKILL)
        vnc_process.wait()

    return not is_running(vnc_process)
