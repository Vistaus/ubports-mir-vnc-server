import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import "../components/aboutpage"
import "../components/common"

BasePage {
    id: aboutPage
    
    title: i18n.tr("About") + " MirVNCServer"
    flickable: aboutFlickable
    
    Flickable {
        id: aboutFlickable
        
        anchors.fill: parent
        contentHeight: listView.height + iconComponent.height + Suru.units.gu(5)
        boundsBehavior: Flickable.DragOverBounds

        ScrollIndicator.vertical: ScrollIndicator { }
        
        function externalLinkConfirmation(link) {
            externalDialog.externalURL = link
            externalDialog.openNormal()
        }
        
        
        IconComponent {
            id: iconComponent
            
            anchors {
                top: parent.top
                topMargin: Suru.units.gu(2)
                left: parent.left
                right: parent.right
            }
        }
        
        ListView {
            id: listView
            
            model: aboutModel
            height: contentHeight
            interactive: false
            
            anchors{
                top: iconComponent.bottom
                topMargin: Suru.units.gu(2)
                left: parent.left
                right: parent.right
            }
            
            section{
                property: "section"
                delegate: SectionItem{text: section}
            }
            
            delegate: NavigationItem{
                title: model.text
                subtitle: model.subText
                iconName: model.icon
                url: model.urlText
            }
        }
        
        ListModel {
            id: aboutModel
            
            Component.onCompleted: fillData()
    
            function fillData(){
                append({"section": i18n.tr("Support"), "text": i18n.tr("Report a bug"), "subText": "", "icon": "mail-mark-important", "urlText": "https://gitlab.com/abmyii/ubports-mir-vnc-server/-/issues"})
                append({"section": i18n.tr("Support"), "text": i18n.tr("Contact Developer"), "subText": "", "icon": "stock_email", "urlText": "mailto:abdurrahmaaniqbal@hotmail.com"})
                append({"section": i18n.tr("Support"), "text": i18n.tr("View source"), "subText": "", "icon": "stock_document", "urlText": "https://gitlab.com/abmyii/ubports-mir-vnc-server"})
                append({"section": i18n.tr("Support"), "text": i18n.tr("Donate to Kugi"), "subText": "", "icon": "like", "urlText": "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=2GBQRJGLZMBCL"})
                append({"section": i18n.tr("Support"), "text": i18n.tr("View in OpenStore"), "subText": "", "icon": "ubuntu-store-symbolic", "urlText": "openstore://mirvncserver.abmyii"})
                append({"section": i18n.tr("Support"), "text": i18n.tr("Other apps by Abdurrahmaan"), "subText": "", "icon": "stock_application", "urlText": "https://open-store.io/?search=author%3AAbdurrahmaan%20Iqbal"})
                append({"section": i18n.tr("Support"), "text": i18n.tr("Other apps by Kugi"), "subText": "", "icon": "stock_application", "urlText": "https://open-store.io/?search=author%3AKugi%20Eusebio"})
                append({"section": i18n.tr("Developers"), "text": "Abdurrahmaan Iqbal", "subText": i18n.tr("Main Developer"), "icon": "", "urlText": "https://gitlab.com/abmyii"})
                append({"section": i18n.tr("Developers"), "text": "Kugi Eusebio", "subText": i18n.tr("Developer"), "icon": "", "urlText": "https://github.com/kugiigi"})
            }
        }
    }
    
    ExternalDialog{
        id: externalDialog
    }
}
