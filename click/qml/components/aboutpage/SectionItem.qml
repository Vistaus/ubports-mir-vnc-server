import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.3

ColumnLayout{
    id: sectionItem
    
    property alias text: label.text
    
    spacing: Suru.units.gu(2)

    anchors {
        left: parent.left
        right: parent.right
    }
    
    
    Label {
        id: label
        
        Layout.topMargin: Suru.units.gu(2)
        Layout.leftMargin: Suru.units.gu(2)
        
        font.weight: Font.Medium
        elide: Text.ElideRight
        color: Suru.foregroundColor
    }
    
    Rectangle{
        id: border
        
        Layout.preferredHeight: Suru.units.gu(0.25)
        Layout.fillWidth: true
        color: Suru.tertiaryForegroundColor
    }
 }   
