mirvncserver
============

(Forked from https://github.com/ycheng/mir-vnc-server)

Currently we don't have VNC server for UBPorts. This is an attempt at creating one.

Much of the keyboard/mouse input code was copied from https://github.com/hanzelpeter/dispmanx_vnc/blob/master/main.c

Rotation code from:
https://github.com/LibVNC/libvncserver/blob/master/examples/rotate.c
